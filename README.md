![logo](https://gitlab.com/CurveML/CurveML-PointNet/-/raw/main/pics/curveml-logo-pointnet-pytorch.png)

# CurveML-PointNet

This is the modified version of the [PointNet/PointNet2 Pytorch](https://github.com/yanx27/Pointnet_Pointnet2_pytorch) repository used to train PointNet models with the CurveML dataset.

## Directories

[data](https://gitlab.com/CurveML/CurveML-PointNet/-/raw/main/data/) - CurveML dataset in dataframe format (no images, only point sets in x,y,z coordinates and GT)

[data_utils](https://gitlab.com/CurveML/CurveML-PointNet/-/raw/main/data_utils/) - Datasets and DataLoaders to load the data in Pytorch

[graphs](https://gitlab.com/CurveML/CurveML-PointNet/-/raw/main/graphs/) - Scripts to generate graphs used in the CurveML papers

[log](https://gitlab.com/CurveML/CurveML-PointNet/-/raw/main/log/) - Output directory for the PointNet training scripts (also cointains code, checkpoints, etc.)

[models](https://gitlab.com/CurveML/CurveML-PointNet/-/raw/main/models/) - Code to create Pytorch PointNet architectures

[notebooks](https://gitlab.com/CurveML/CurveML-PointNet/-/raw/main/notebooks/) - Evaluation notebooks to assess model performances and to visualize results

[pics](https://gitlab.com/CurveML/CurveML-PointNet/-/raw/main/pics/) - Just the CurveML logo

[screenshots](https://gitlab.com/CurveML/CurveML-PointNet/-/raw/main/screenshots/) - Screenshots of dataset and predictions captured during data analysis and training

[stdout-logs](https://gitlab.com/CurveML/CurveML-PointNet/-/raw/main/stdout-logs/) - Logs of the training scripts (captured from standard output)

[visualizer](https://gitlab.com/CurveML/CurveML-PointNet/-/raw/main/visualizer/) - Code to visualize point cloud datasets (not used for CurveML visualization)

## Files

[train_classification.py](https://gitlab.com/CurveML/CurveML-PointNet/-/raw/main/train_classification.py) - Training script for the classification task

[train_regression.py](https://gitlab.com/CurveML/CurveML-PointNet/-/raw/main/train_regression.py) - Training script for the regression task

[test_classification.py](https://gitlab.com/CurveML/CurveML-PointNet/-/raw/main/test_classification.py) - Python script to test the pretrained model on the classification task

[test_regression.py](https://gitlab.com/CurveML/CurveML-PointNet/-/raw/main/test_regression.py) - Python script to test the pretrained models on the regression tasks

## Evaluation Notebooks

[Classification](https://htmlpreview.github.io/?https://gitlab.com/CurveML/curveml-pointnet/-/raw/main/notebooks/html/geometric-primitives-classification-v1.2-pointnet-classification-evaluation-dataset-v1.0-wo-splines.html?inline=true)

[Regression - angle](https://htmlpreview.github.io/?https://gitlab.com/CurveML/curveml-pointnet/-/raw/main/notebooks/html/geometric-primitives-classification-v1.2-pointnet-regression-evaluation-dataset-v1.0-wo-splines-angle.html?inline=true)

[Regression - n_petals](https://htmlpreview.github.io/?https://gitlab.com/CurveML/curveml-pointnet/-/raw/main/notebooks/html/geometric-primitives-classification-v1.2-pointnet-regression-evaluation-dataset-v1.0-wo-splines-n_petals.html?inline=true)


[Regression - a](https://htmlpreview.github.io/?https://gitlab.com/CurveML/curveml-pointnet/-/raw/main/notebooks/html/geometric-primitives-classification-v1.2-pointnet-regression-evaluation-dataset-v1.0-wo-splines-a.html?inline=true)

[Regression - b](https://htmlpreview.github.io/?https://gitlab.com/CurveML/curveml-pointnet/-/raw/main/notebooks/html/geometric-primitives-classification-v1.2-pointnet-regression-evaluation-dataset-v1.0-wo-splines-b.html?inline=true)


[Regression - trans_x](https://htmlpreview.github.io/?https://gitlab.com/CurveML/curveml-pointnet/-/raw/main/notebooks/html/geometric-primitives-classification-v1.2-pointnet-regression-evaluation-dataset-v1.0-wo-splines-trans_x.html?inline=true)

[Regression - trans_y](https://htmlpreview.github.io/?https://gitlab.com/CurveML/curveml-pointnet/-/raw/main/notebooks/html/geometric-primitives-classification-v1.2-pointnet-regression-evaluation-dataset-v1.0-wo-splines-trans_y.html?inline=true)

